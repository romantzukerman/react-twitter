import { TWEETS_FETCHED, QUERY_PASSED } from '../actions/actions.js'
import { combineReducers } from 'redux'

const tweets = (state = [], action) => {
    switch (action.type) {
        case TWEETS_FETCHED:
          return Object.assign({}, state, {
            tweets: action.tweets
          })
        default:
          return state
    }
} 

const query = (state = "", action) => {
    switch (action.type) {
        case QUERY_PASSED:
          return Object.assign({}, state, {
            query: action.query
          })
        default:
          return state
    }
}

const rootReducer = combineReducers({
  tweets,
  query
})

export default rootReducer