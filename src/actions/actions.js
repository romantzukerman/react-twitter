export const TWEETS_FETCHED = 'TWEETS_FETCHED'
export const QUERY_PASSED = 'QUERY_PASSED'

export const fetchTweets = data => ({
    type: TWEETS_FETCHED,
    tweets: data
})

export const queryPassed = data => ({
    type: QUERY_PASSED,
    query: data
})