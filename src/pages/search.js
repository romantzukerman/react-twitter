import React from 'react';
import Content from '../components/content/';

class Search extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      query: ''
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.location.search != nextProps.location.search) {
      this.setState({query: nextProps.location.search})
    }
  }

  render () {
    const queryString = require('query-string');
    const searchTerm = queryString.parse(this.props.location.search).q
    return (
      <Content query={searchTerm} />
    )
  }

}


export default (Search);