import { fetchTweets } from '../actions/actions.js';

const getTweets = (query) => {

    const twitterAPI = 'http://localhost:7890/1.1' 
    const queryEncoded = encodeURIComponent(query)
    return (dispatch) => {
    return fetch(`${twitterAPI}/search/tweets.json?q=${queryEncoded}`, {
        headers: {
        Accept: 'application/json',
        'Access-Control-Request-Headers': 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers'
        }
    })
    .then((response) => response.json())
    .then(res => {
        dispatch(fetchTweets(res.statuses))
    })
    }
}

export default getTweets