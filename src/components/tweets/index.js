import React from 'react';

import Tweet from '../../components/tweet/';

import getTweets from '../../helpers/api.js';

import { connect } from "react-redux";

class Tweets extends React.Component {
constructor(props) {
  super(props)
}

  componentDidMount() {
    this.props.dispatch(getTweets(this.props.query))
  }

  componentDidUpdate(prevProps) {  
    if (this.props.query !== prevProps.query) {
      this.props.dispatch(getTweets(this.props.query))
    }
    
  } 

  render () {
    
    const tweets = this.props.tweets.tweets

    if(tweets == undefined)
      return null

    return (
      
      <div>
              {
              tweets.map((tweet, i) => 
                <Tweet key={i} tweet={tweet} />
              )
            }
      </div>
    )
  }

}

const mapStateToProps = state => ({
    tweets: state.tweets
  }
);


export default connect(mapStateToProps)(Tweets)