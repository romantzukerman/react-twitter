import React from 'react';
import './tweet.css';

class Tweet extends React.Component{

    render() {

        const tweet = this.props.tweet;

        return (     
        <article className='single-tweet'>
            <div className='avatar'>
                <img className='avatar__img' src={tweet.user.profile_image_url}/>
            </div>
            <div className='user-info'>
                <h4 className='name'>{tweet.user.screen_name}</h4>
                <p className='username'>{tweet.user.name}</p>
            </div>
            <div className='tweet-text'>
                <p>
                    {tweet.text}
                </p>
            </div>        
        </article>     
        )
        }

}

export default Tweet;