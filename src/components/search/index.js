import React from 'react';
import Button from 'react-bootstrap/Button';

import { withRouter } from 'react-router-dom';
import { queryPassed } from '../../actions/actions.js';

import { connect } from "react-redux";

import './search.css'

class SeachBar extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            query: ''
        }
    }


    submitForm = (e) => {
        e.preventDefault()
        
        this.props.dispatch(queryPassed(this.state.query))
        this.props.history.push(`/search?q=${encodeURIComponent(this.state.query)}`)
    }  
    
    formHandler = (e) => {
        this.setState({ query: e.target.value })
    }

    render() {
        return (
            <div> 
                <form className='search' onSubmit={this.submitForm}>
                    <input type='search' placeholder="Enter keywords" onChange={this.formHandler} value={this.state.query} />
                    <Button type='submit'>Search</Button>
                </form>
            </div>
        )
    }

}

export default connect()(withRouter(SeachBar))