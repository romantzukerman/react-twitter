import React from 'react'
import SearchBar from '../search/'

import './header.css'

import logo from '../../../src/logo.svg';

const Header = () => {
    return (
        <div className='header'>
            <a href='/' className='logo'><img src={logo}></img></a>        
            <SearchBar/>
        </div>        
    )
}

export default Header;