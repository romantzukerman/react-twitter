import React from 'react';

import Header from '../../components/header/';
import Tweets from '../../components/tweets/';

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';


class Content extends React.Component {
constructor(props) {
    super(props)
}
render () {

  const showSearchTitle = (this.props.query.length > 0) ? true : false;

  return (
      <div>
        <Container>
          <Row>
            <Col>
              <Header/>
            </Col>
          </Row>

          <Row>
            <Col md={{span: 6, offset: 3}} xs={12}>

              {
                showSearchTitle &&
                <h3>Search results for: <strong>{this.props.query}</strong></h3>
              }  

              <Tweets query={this.props.query}/>

            </Col>
          </Row>
        </Container>
      </div>
    )
  }
}

export default (Content);