import React from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import Index from './pages/index.js';
import Search from './pages/search.js';

import './App.css';

class App extends React.Component {

  render () {
    return (
      <Router>
        <Switch>
          <Route exact path='/' component={Index} />
          <Route path='/search' component={Search} />            
        </Switch>
      </Router>
    )
  }
}


export default App;
